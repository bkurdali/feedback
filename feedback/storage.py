#!/bin/python3
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali


if "pickle" in locals():
    import importlib
    importlib.reload(constants)

else:
    from . import constants

import pickle
import hashlib
import mimetypes
import io
import shutil
import uuid
import os
from queue import Queue

from .constants import _, LocalPaths
from googleapiclient.errors import HttpError
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

CHUNK_SIZE = 1024 * 1024


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


class DummyStorage:

    def __init__(self):
        pass

    def create_folder(self, name, parent):
        return

    def is_folder(self, file):
        return True

    def ls(self, folder):
        return

    def download_file(self, remote_file):
        pass

    def download_folder(self, folder):
        pass

    def add_file(self, local_file):
        pass

    def purge_local_folder(self, folder_path):
        shutil.rmtree(folder_path)

    def setup_folder(self, name, folder_structure, parent_folder):
        pass

    def setup_drive(self, course):
        pass

    def rm(self, remote_path):
        pass


class GoogleStorage:
    """ wrapper for the google drive api """

    scopes = ['https://www.googleapis.com/auth/drive',]
    token_path = f"{LocalPaths.lib}token.pickle"
    cred_path = f"{LocalPaths.lib}credentials.json"

    def __init__(self):
        """ initialize a google service we can can access """
        self.creds = None
        if os.path.exists(self.token_path):
            with open(self.token_path, 'rb') as token:
                self.creds = pickle.load(token)
        if self.creds is None or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                self.creds.refresh(Request())
            else:
                self.flow = InstalledAppFlow.from_client_secrets_file(
                    self.cred_path, self.scopes)
                self.creds = self.flow.run_local_server(port=0)
            with open(self.token_path, 'wb') as token:
                pickle.dump(self.creds, token)
        self.service = build('drive', 'v3', credentials=self.creds)
        self.classes = {}
        self.download_queue = Queue()
        self.download_count = 0
        self.upload_queue = Queue()
        self.upload_count = 0

    def create_folder(self, name, parent_id, drive_id=None):
        """ create a folder in a parent, and return its ID """
        # don't make a duplicate
        files = self.ls(parent_id, drive_id)
        folders = [f for f in files if self.is_folder(f)]
        for folder in folders:
            if folder.get('name') == name:
                return folder.get('id')
        # create if it doesn't exist
        metadata = {
            'name': name,
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [parent_id]
            }
        folder = self.service.files().create(
            body=metadata,
            fields='id',
            supportsAllDrives=True,
            ).execute()
        return folder.get('id')

    def create_drive(self, drive):
        """ create a new shared drive and return it """
        results = self.service.drives().list().execute()
        items = results.get('drives', [])
        matches = None
        if items:
            matches = [d for d in items if d['name'] == drive]
            if matches:
                return matches[0]['id']

        drive_metadata = {'name': drive}
        request_id = str(uuid.uuid4())
        drive_id = self.service.drives().create(
            body=drive_metadata,
            requestId=request_id,
            fields='id'
            ).execute()
        return drive_id['id']

    def is_folder(self, file_data):
        return file_data['mimeType'] == 'application/vnd.google-apps.folder'

    def ls(self, folder_id, drive_id=None):
        """ list folder files """
        if drive_id is None:
            try:
                results = self.service.files().list(
                    q=f"'{folder_id}' in parents and trashed != true",
                    corpora='allDrives',
                    includeItemsFromAllDrives=True,
                    supportsAllDrives=True,
                    fields='files(id, name, mimeType, md5Checksum)'
                        ).execute()
            except HttpError:
                print("error", folder_id)
                results = self.service.files().list(
                    q=f"'{folder_id}' in parents and trashed != true",
                    corpora='allDrives',
                    includeItemsFromAllDrives=True,
                    supportsAllDrives=True,
                    fields='files(id, name, mimeType, md5Checksum)'
                        ).execute()
        else:
            results = self.service.files().list(
                q=f"'{folder_id}' in parents and trashed != true",
                corpora='drive',
                driveId=drive_id,
                includeItemsFromAllDrives=True,
                supportsAllDrives=True,
                fields='files(id, name, mimeType, md5Checksum)'
                    ).execute()
        return results.get('files')

    def id_from_path(self, parent_id, path):
        path_list = path.strip('/').split('/')
        id = parent_id
        for folder in path_list:
            print("name",folder)
            folders = [
                f for f in self.ls(id) if self.is_folder(f) and f["name"] == folder
                ]
            print(folders)
            id = folders[0]["id"]
        return id

    def download_file(self, file_id, file_name, local_path, use_chunks=True):
        request = self.service.files().get_media(fileId=file_id)
        local_file = os.path.join(local_path, file_name)
        fh = io.FileIO(local_file, mode='wb')
        if use_chunks:
            downloader = MediaIoBaseDownload(fh, request,chunksize=CHUNK_SIZE)
        else:
            downloader = MediaIoBaseDownload(fh, request)
        print("downloading", file_name)
        return downloader

    def queue_download_file(self, file_id, file_name, local_path):
        self.download_count += 1
        self.download_queue.put_nowait(
            [self.download_count, file_id, file_name, local_path])
        return self.download_count - 1 # index

    def download_queued_file(self, index):
        self.download_count -=1
        idx, file_id, file_name, local_path = self.download_queue.get_nowait()
        idx = idx - 1
        if index != idx:
            print("MISMATCH")
        return download_file(self, file_id, file_name, local_path)

    def check_download(self, downloader, fails=[]):
        print("CHECKING DOWNLOAD")
        try:
            status, done = downloader.next_chunk()
        except HttpError:
            print("ERROR DOWNLOADING")
            fails.append('error')
            return False
        else:
            if not done:
                print("STILL DOWNLOADING")
            else:
                print("DONE DOWNLOADING")
        return done

    def download_folder(
            self, folder_id, local_path,
            downloaders=[], filetypes=[], drive_id=None, use_chunks=True):
        # grab movie and .blend files from a folder
        file_list = self.ls(folder_id, drive_id)
        for file_data in file_list:
            if self.is_folder(file_data):
                sub_path = LocalPaths.check(
                    os.path.join(local_path, file_data['name']))
                downloaders.extend(
                    self.download_folder(
                        file_data['id'], sub_path, downloaders,
                        filetypes, drive_id, use_chunks
                        )
                    )
            else:
                if (
                        filetypes and
                        file_data['mimeType'] in filetypes
                        ) or not filetypes:
                    local_file = os.path.join(local_path, file_data['name'])
                    if os.path.isfile(local_file):
                        local_md5 = md5(local_file)
                        remote_md5 = file_data.get('md5Checksum', None)
                        do_download = local_md5 != remote_md5

                    else:
                        do_download = True
                    if do_download:
                        downloaders.append(
                            self.download_file(
                                file_data['id'],
                                file_data['name'],
                                local_path, use_chunks=use_chunks)
                            )

        print("exiting download folder")
        return downloaders

    def add_file(self, filepath, parent_id):
        """ Add feedback file to the assignment folder """
        #TODO check if a file of the same name exists
        filename = os.path.split(filepath)[-1]
        mimetype = mimetypes.MimeTypes().guess_type(filename)[0]
        metadata = {
            'name': filename,
            'parents': [parent_id]
            }
        media = MediaFileUpload(filepath,
                        mimetype=mimetype,)#                        resumable=True)
        print(metadata)
        print(media)
        print(mimetype)
        remotefile = self.service.files().create(body=metadata,
                                    media_body=media,
                                    supportsAllDrives=True,
                                    fields='id').execute()
        return remotefile.get('id')

    def purge_local_folder(self, folder_path):
        shutil.rmtree(folder_path)

    def setup_folder(self, key, folder_structure, parent_id, drive_id=None):
        name = folder_structure.get(_.name, key)
        print("setting up ", name)
        if folder_structure[_.id] == [""]:
            print("didn't find")
            folder_structure[_.id][0] = self.create_folder(
                name, parent_id, drive_id)
            print("made", folder_structure[_.id])
        subfolders = folder_structure.get(_.subfolders, {})
        for subfolder, structure in subfolders.items():
            self.setup_folder(
                subfolder, structure, folder_structure[_.id][0], drive_id)

    def setup_drive(self, course, id=None):
        """ don't know about structure; derive from data """

        drive_name = f"{course[_.term]}_{course[_.number]}"
        if id is None:
            drive_id = self.create_drive(drive_name)
        else:
            drive_id = id
        drive_structure = course.get(_.subfolders, {})
        for folder in drive_structure.keys():
            self.setup_folder(folder, drive_structure[folder], drive_id)
        return drive_id

    def get_metadata(self, node_id):
        return self.service.files().get(
            fileId=node_id,
            supportsAllDrives=True,
            fields="kind, parents, id, driveId, name, md5Checksum"
            ).execute()

    def duplicate_check(self, node_id):
        metadata = self.get_metadata(node_id)
        # print(metadata)
        parents = metadata['parents']
        name = metadata['name']
        dups = set()
        # print(name)
        for parent in parents:
            siblings = self.ls(parent)
            # print(name, siblings)
            for s in siblings:
                # print("   ", s.get('name'))
                if s['name'] == name:
                    print("match", name, node_id, s['id'])
                    if s['id'] != node_id:
                        print("added")
                        dups.add(s['id'])
        return dups

    def rm(self, node_id):
        """ removes a node and all it's children """
        response = self.service.files().delete(
            fileId=node_id,
            supportsAllDrives=True,
            ).execute()
        return response
