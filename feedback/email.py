# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C)  2019 Bassam Kurdali

if "bpy" in locals():
    import importlib
    importlib.reload(constants)
    importlib.reload(utils)
else:
    from . import constants
    from . import utils

import bpy
import random
import keyring
import smtplib
from .constants import _
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# TODO urls for help with this message (pdf, movie)
NO_FILE = [
    (
        "[{scene.course}] Couldn't find {type.title()[:-1]} {number} video",
        """Dear {name},

If you are auditing the class you can safely ignore the rest of this message.

I couldn't find your final render for {type} {number} on the {course_name}
class drive.
If you're having trouble, don't hesitate to email me, otherwise, please upload
a .mp4 file to the appropriate folder in the course shared google drive:
{drive}/Student Folders/{name}/{type.upper()}s/{type.upper()[0]}{number}

If you're having trouble rendering a movie from blender, check
https://drive.google.com/open?id=1k1Z_5v2Z07n3okG-PX8bflsgI5CzZqgu

Again, if you have any other concerns, please contact me,
Cheers,
Bassam
"""),]

UPLOADED = [
    (
        "[{scene.course}] Uploaded {type.title()[:-1]} {number} feedback video",
        """Dear {name},

I just uploaded a feedback video into:
{drive}/Student Folders/{name}/{type.upper()}s/{type.upper()[0]}{number}

Congrats on finishing the assignment! I hope you enjoyed it.

Cheers,
Bassam
"""),
    (
        "[{scene.course}] Uploaded {type.title()[:-1]} {number} feedback video",
        """Hi {name},

I just uploaded your feedback video into:
{drive}/Student Folders/{name}/{type.upper()}s/{type.upper()[0]}{number}

Great job!

Cheers,
Bassam
"""),]

def store_password(self, value):
    keyring.set_password("feedback", self.user, value)

def get_password(self):
    return keyring.get_password("feedback", self.user)

class Send:
    """ Wrapper for email and smtplib """

    url = 'smtp.hampshire.edu'
    port = 587

    def __init__(self, prefs, to, subject, body):
        self.user = prefs.user
        self.address = prefs.address
        self.url = prefs.url
        self.port = prefs.port

        self.password = get_password(self)
        if not self.password:
            self.password = password
            store_password(self, password)

        msg = MIMEMultipart()
        msg['From'] = self.address
        msg['To'] = to
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        self.msg = msg.as_string()
        self.to = to

    def execute(self):
        server = smtplib.SMTP(self.url, self.port)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(self.user, self.password)
        problems = server.sendmail(self.address, self.to, self.msg)
        server.quit()
        return problems

def message_student(context, student, message):
    scene = context.scene

    type = context.scene.work_type
    number = context.scene.work_number
    name = student[_.name].split()[0] # XXX need to do better
    drive = f"{scene.term}_{scene.course}"
    course = utils.get_course(scene.course, scene.term)
    course_name = course[_.name]
    subject = eval(f'f"""{message[0]}"""')
    body = eval(f'f"""{message[1]}"""')
    email = student[_.email]

    Send(
        context.preferences.addons["feedback"].preferences,
        email, subject, body).execute()


def not_found(context, student):
    message_student(context, student, random.choice(NO_FILE))


def uploaded(context, student):
    message_student(context, student, random.choice(UPLOADED))    
