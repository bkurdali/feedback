# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali

if "bpy" in locals():
    import importlib
    importlib.reload(storage)
    importlib.reload(constants)
    importlib.reload(test)
else:
    from . import storage
    from . import constants
    from . import test

import bpy
import json
import os
import datetime
import enum
import sys
import requests

from .storage import GoogleStorage as Storage
from .constants import _, LocalPaths
"""
Course data structures, helpers and scrapers
"""

if test.testing:
    CONFIG_FILE = "test.json"
else:
    CONFIG_FILE = "config.json"


class Term():
    """ utililty to figure out current term, convert between string forms """
    spring = (1, 8)
    hamp_founded = 70

    def from_datetime(self, date_time):
        month = date_time.day
        self.year = date_time.year
        if month in range(self.spring[0], self.spring[1]):
            self.term = "S"
        else:
            self.term = "F"

    def from_string(self, termstring):
        self.term = termstring[-1]
        self.year = int(termstring[:-1])
        if self.year < self.hamp_founded:
            self.year += 2000
        elif self.year < 100:
            self.year += 1900

    def __init__(self, *args):
        if len(args) == 0:
            self.from_datetime(datetime.date.today())
        else:
            if type(args[0]) is datetime.date:
                self.from_datetime(args[0])
            elif type(args[0]) is str:
                self.from_string(self, termstring)

    def expanded(self):
        return f"{self.year}{self.term}"

    def contracted(self):
        return f"{str(self.year)[-2:]}{self.term}"

    __str__ = __repr__ = expanded


class Hub:
    """ Hold actual courses data, save/load to json """

    config_file = os.path.join(LocalPaths.config, CONFIG_FILE)
    term = str(Term())
    terms = list({(term,) * 3,})
    if os.path.isfile(config_file):
        hub = json.loads(open(config_file).read())
        __c_terms__ = list({(c[_.term],) * 3 for c in hub.get(_.courses, [])})
        for __term__ in __c_terms__:
            if not __term__ in terms:
                terms.append(__term__)
        terms.sort(key=lambda x: int(x[0][:-1]) + {'S':0, 'F':0.5}[x[0][-1]])
    else:
        hub = {_.term: term, _.courses:[],}

    @classmethod
    def read(cls):
        if os.path.isfile(cls.config_file):
            cls.hub = json.loads(open(cls.config_file).read())
        else:
            cls.write()
        cls.terms_update()

    @classmethod
    def write(cls):
        cls.terms_update()
        open(cls.config_file, 'w').write(json.dumps(cls.hub))

    @classmethod
    def terms_update(cls):
        cls.terms.extend(list({
            (c[_.term], ) * 3 for c in cls.hub.get(_.courses, [])
            if c[_.term] not in cls.terms}))
        cls.terms = list(set(cls.terms))

    @classmethod
    def set_current_term(cls, index):
        cls.hub[_.term] = cls.term = cls.terms[index][0]
        cls.write()

    @classmethod
    def latest_term(cls):
        return max(
                cls.terms,
                key=lambda x: int(x[0][:-1]) + {'S':0, 'F':0.5}[x[0][-1]]
                )[0]

    @classmethod
    def term_courses(cls, self, context):
        return [
            (c[_.number], c[_.number], c[_.name])
            for c in cls.hub[_.courses] if c[_.term] == cls.term]

    @classmethod
    def find_course(cls, term, number, courses=[]):
        """ find a course by number and term """
        if not courses:
            print("didn't get courses")
            courses = cls.hub.get(_.courses, [])
            print("got", len(courses))
        for course in courses:
            print(course[_.term], course[_.number])
            if all(
                    course[key] == value for key, value in {
                        _.term: term,
                        _.number: number}.items()
                        ):
                print('GOTTT', course[_.term], course[_.number])
                return course
        return {}

    @classmethod
    def find_student(cls, name, course):
        """ find a student by name """
        roster = course.get(_.roster, [])
        for student in roster:
            if student[_.name] == name:
                return student
        return {}

    @classmethod
    def add_student(cls, course, student_data, write=True):
        """ adds a student to an existing course """
        course[_.roster].append(student_data)
        drive_id = course.get(_.id, [""])
        if drive_id != [""]:
            cls.build_student_folder(student_data, course)
            name = student_data[_.name]
            print(name)
            Storage().setup_folder(
                name,
                course[_.subfolders][_.roster][_.subfolders][name],
                course[_.subfolders][_.roster][_.id][0],
                drive_id[0]
                )
        if write:
            cls.write()

    @classmethod
    def drop_student(cls, course, name, drive=False):
        """ drop a student from the class """
        for i, student in enumerate(course[_.roster]):
            if student[_.name] == name:
                print('dropping', name)
                course[_.roster].pop(i)
                break
        if name in course[_.subfolders][_.roster][_.subfolders]:
            print('forgetting ', name, ' storage')
            course[_.subfolders][_.roster][_.subfolders].pop(name)
        # cls.write()

    @classmethod
    def add(cls, page, assignments, workshops, milestones):
        """ Load a roster page into a new or existing course """
        students, term, number, name = cls.get_course_data(
            page, LocalPaths.images)
        course = cls.find_course(term, number)
        if not course:
            course[_.roster] = students
            course[_.term] = term
            course[_.number] = number
            cls.hub[_.courses].append(course)
            cls.terms_update()
        else:
            if _.roster not in course:
                course[_.roster] = []
            for student_data in students:
                student = cls.find_student(student_data[_.name], course)
                if student:
                    # update data for existing student
                    for key, value in student_data.items():
                        student[key] = value
                else:
                    # add new student to course
                    cls.add_student(course, student_data, write=False)

        course[_.name] = name
        course[_.TA] = [] # TODO get TA list from the hub
        course[_.assignments] = assignments
        course[_.workshops] = workshops
        course[_.milestones] = milestones
        registered = [s[_.name] for s in students]
        removes = []
        for student in course[_.roster]:
            if student[_.name] not in registered:
                removes.append(student[_.name])
        for remove in removes:
            cls.drop_student(course, remove)
        cls.setup_drive(course)

    @classmethod
    def update_work_counts(
            cls, term, number, assignments, workshops, milestones):
        course = cls.find_course(term, number)
        if not course:
            return
        course[_.assignments] = assignments
        course[_.workshops] = workshops
        course[_.milestones] = milestones
        cls.setup_drive(course)

    @classmethod
    def setup_drive(cls, course):
        subfolders = course.get(_.subfolders, None)
        if not subfolders:
            cls.build_course_folders(course)
        drive_id = course.get(_.id, [""])
        if drive_id == [""]:
            course[_.id][0] = Storage().setup_drive(course)
        cls.write()

    @classmethod
    def update_drive(cls, course):
        subfolders = course.get(_.subfolders, None)
        assignments = course.assignments
        return

    @classmethod
    def setup_test_data(cls):
        """ setup drive folder for courses in test json """
        for course in cls.hub[_.courses]:
            cls.setup_drive(course)

    config = { # Keys here are an interface; do't change them willy-nilly
        _.term: "2020S",
        _.courses: [
            {
                _.name: "e.g. Animation Fundamentals",
                _.number: "e.g. CS197",
                _.term: "e.g. 2020S",
                _.id: ["Storage ID"],
                _.subfolders: {}, # subfolder structure
                _.roster: [
                        {
                        _.name: "e.g. Jo Doe",
                        _.pronouns: "e.g. They/Them",
                        _.email: "e.g. d100@hampshire.edu",
                        _.division: "e.g. d3/mt holyoke/etc",
                        _.term:"e.g. 18F",
                        _.image_path: "e.g. path/to/image",
                        _.image_url: "e.g. https://sub.domain/path/to/image",
                        _.advisor: "e.g. Bassam Kurdali",
                        _.advisor_email: "e.g. mail@hampshire.edu",
                        _.assignments:[],
                        _.workshops:[],
                        _.milestones:[], # 0,1,0,2 etc. based on revisions
                        _.id: ["Storage ID"], # same in folder structure
                        },
                    ],
                _.TA: [],
                _.assignments:4,
                _.workshops:5,
                _.milestones:3,
                },
            ],
        }

    def multiples(term, subfolders={}):
        return {
            _.name: term.title(),
            _.ref: term,
            _.type: _.count,
            _.prefix: term.title()[0],
            _.subfolders: subfolders
            }

    subfolders = {   # This is just for drive structure
        "Final Project": {
            _.type: "",
            _.subfolders: {_.milestones: multiples(_.milestones)}
            },
        _.roster: {
            _.name: "Student Folders",
            _.type: _.roster,
            _.ref: _.roster,
            _.subfolders: {}
            },
        'surveys': {},
        'screen recordings': {},
        }

    # this will create e.g. A1 A2 etc in Assignments eventually
    for __term__ in [_.assignments, _.workshops]:
        subfolders[__term__] = multiples(__term__)

    # this will duplicate the Assignments/etc structure for each student folder
    for __term__ in [_.assignments, _.workshops, "Final Project"]:
        subfolders[_.roster][_.subfolders][__term__] = subfolders[__term__]

    @classmethod
    def build_subfolder(cls, key, structure, course):

        # these determine what we do for subfolders
        type = structure.get(_.type, "")
        roster = course.get(_.roster, [])
        def defined_subfolders(cls, structure):
            s = structure.get(_.subfolders,{})
            return {k: cls.build_subfolder(k, v, course)for k, v in s.items()}

        # folder dict keys
        id = [""]
        name = structure.get(_.name, key)
        subfolders = {} # Fill below!

        # build the subfolders based on type
        if type is _.roster:
            for student in roster:
                student[_.id] = student_id = [""] # same drive ID for both
                subfolders[student[_.name]] = {
                    _.id: student_id,
                    _.name: student[_.name],
                    _.subfolders: defined_subfolders(cls, structure)
                   }

        elif type is _.count:
            ref = structure.get(_.ref, "")
            count = course.get(ref, 0)
            prefix = structure.get(_.prefix, "")
            for i in range(count):
                sub_key = f"{prefix}{i + 1}"
                subfolders[sub_key] = {
                    _.name:sub_key,
                    _.id:[""],
                    _.subfolders: defined_subfolders(cls, structure)
                   }
        else:
            subfolders = defined_subfolders(cls, structure)
        return {_.id: id, _.name: name, _.subfolders: subfolders}

    @classmethod
    def build_all_folders(cls, config):
        """
        modifies config in place to add folder structure definition and
        placeholder ids
        """
        for course in config.get(_.courses,[]):
            cls.build_course_folders(course)
        return

    @classmethod
    def build_student_folder(cls, student, course):
        """ add drive structure for new student """
        parent_folder = course[_.subfolders][_.roster][_.subfolders]
        student[_.id] = id = student.get(_.id, [""])
        key = student[_.name]
        value = {
            _.name: key,
            _.id: id,
            _.subfolders: {
                k: cls.build_subfolder(k, v, course)
                for k, v in cls.subfolders[_.roster][_.subfolders].items()
                }
             }
        parent_folder[key] = value

    @classmethod
    def build_course_folders(cls, course):
        """
        modifies course in place to add folder structure definition and
        placeholder ids
        """

        def dictate(source, target):
            for key in source:
                if not key in target:
                    target[key] = source[key] # complete
                else:
                    if _.subfolders in source:
                        if _.subfolders in target:
                            dictate(source[_.subfolders], target[_.subfolders])
                        else:
                            target[_.subfolders] = source[_.subfolders]

        course[_.id] = course.get(_.id, [""])
        subfolders = {
            key: cls.build_subfolder(key, value, course)
            for key, value in cls.subfolders.items()
            }
        if _.subfolders in course:
            dictate(subfolders, course[_.subfolders])
        else:
            course[_.subfolders] = subfolders

    @classmethod
    def refresh(cls):
        cls.hub = cls.read()

    @classmethod
    def term_courses(cls, term):
        return [
            (c[_.number], c[_.name], c[_.name])
            for c in cls.hub[_.courses] if c[_.term] is term]

    @classmethod
    def get_course_data(cls, page, images_path):
        """
        Really dumb, fallible parser for student roster.
        Will break on Hub updates
        """
        class PS(enum.Enum):
            """ parser state """
            searching = 0
            getting = 10
            courseinfo = 20
            coursedata = 30
            coursetitle = 40

        # iportant lines/line beginnings
        roster_div_start = (
            "&lt;div class=&quot;rosterstudent even&quot;&gt;",
            "&lt;div class=&quot;rosterstudent odd&quot;&gt;")
        roster_image = ("&lt;img class=&quot;pic&quot;",)
        roster_contact = ("&lt;div class=&quot;name&quot;&gt;",)
        roster_advisor = (
            "&lt;div class=&apos;advisor&apos;&gt;",
            "&lt;/div&gt;  &lt;div class=&apos;advisor&apos;&gt;")
        roster_pronouns = ("&lt;div class=&quot;pronoun&quot;&gt;",)
        roster_division = ("&lt;div class=&quot;division&quot;&gt;",)
        roster_div_end = ("&lt;/div&gt; &lt;!-- end roster --&gt;",)
        course_div_start = ("&lt;DIV ID=&apos;courseinfo&apos;",)
        course_title = ("&lt;H2&gt;Course&lt;/H2&gt;",)

        # initialize data so we can return something
        students = []
        term = ""
        number = ""
        title = ""

        # get the page saved to disk as a garbagy html file
        with open(page) as webpage:
            lines = webpage.readlines()

        # state engine
        state = PS.searching
        for line in lines:
            line = line.lstrip().rstrip()
            if state is PS.searching:
                if line in roster_div_start:
                    students.append({})
                    state = PS.getting
                elif any(line.startswith(a) for a in course_div_start):
                    state = PS.courseinfo
            elif state is PS.courseinfo:
                if line in course_title:
                    state = PS.coursedata
            elif state is PS.coursedata:
                course_data = line.split("&lt;B&gt;")[1].split("&lt;/B&gt;")[0]
                term, number = course_data.split(" ")
                state = PS.coursetitle
            elif state is PS.coursetitle:
                title = line.split("&lt;BR/&gt;")[0]
                state = PS.searching
            elif state is PS.getting:


                if any(line.startswith(l) for l in roster_image):
                    link = line.split("src=&quot;")[-1].split("&quot;")[0]
                    students[-1][_.image_url] = link
                    name = link.replace("https","http").split('/')[-1]
                    # download the image and save it and a pointer to it
                    response = requests.get(link,verify=True)
                    local_path = os.path.join(images_path, name)
                    with open(local_path, 'wb') as image_file:
                        image_file.write(response.content)
                    students[-1][_.image_path] = local_path

                elif any(line.startswith(l) for l in roster_contact):
                    students[-1][_.email] = line.split(
                        "&quot;mailto:")[-1].split("&quot;")[0]
                    name_year = line.split(
                        f'{students[-1][_.email]}'
                        f'&quot;&gt;&lt;/a&gt;')[-1].lstrip().split(
                            "&lt;/div&gt;")[0]
                    name_year = name_year.split(',')
                    students[-1][_.name] = name_year[0].rstrip()
                    students[-1][_.term] = name_year[-1].lstrip()

                elif any(line.startswith(l) for l in roster_advisor):
                    students[-1][_.advisor_email] = line.split(
                        "alt=&apos;")[-1].split("&apos;")[0]
                    if all(m in students[-1][_.advisor_email] for m in ('@','.')):
                        students[-1][_.advisor] = line.split(
                            f'alt=&apos;{students[-1][_.advisor_email]}'
                            f'&apos;&gt;&lt;/a&gt;')[-1].lstrip().split(
                                "&lt;/div&gt;")[0]
                    else:
                        students[-1][_.advisor] = ''
                        students[-1][_.advisor_email] = ''

                elif any(line.startswith(l) for l in roster_division):
                    students[-1][_.division] = line.split(
                        'alt=&apos;')[-1].split('&apos;')[0]

                elif any(line.startswith(l) for l in roster_pronouns):
                    students[-1][_.pronouns] = line.split(roster_pronouns[0])[-1]

                elif line in roster_div_end:
                    state = PS.searching

        return students, term, number, title

    @classmethod
    def purge_duplicate_folders(cls):
        storage = Storage()
        def get_ids(folder, ids):
            ids.append(folder[_.id][0])
            for subfolder in folder.get(_.subfolders, {}).values():
                get_ids(subfolder, ids)
        ids = []
        for course in cls.hub.get(_.courses, []):
            print(course[_.name])
            for subfolder in course[_.subfolders].values():
                get_ids(subfolder, ids)
        dups = set()
        print(len(ids))
        for i, id in enumerate(ids):
            # print (i)
            res = storage.duplicate_check(id)
            dups = dups.union(res)
        # print ("dups", res, dups)
        for dup in dups:
            storage.rm(dup)
        return dups


def current_term_courses(self, context):
    """ needs to be a function and not a class method """
    return [
        (c[_.number], c[_.number], c[_.name])
        for c in Hub.hub[_.courses] if c[_.term] == Hub.term]

def all_terms(self, context):
    return Hub.terms   
