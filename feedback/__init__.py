# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali

bl_info = {
    "name": "Feedback",
    "author": "Bassam Kurdali",
    "version": (1, 2),
    "blender": (2, 80, 0),
    "location": "Sequence Editor",
    "description": "Give Student Feedback",
    "warning": "",
    "wiki_url": "",
    "category": "Editing",
}

if "bpy" in locals():
    import importlib
    importlib.reload(screencast)
    importlib.reload(utils)
    importlib.reload(email)
    importlib.reload(hub)
    importlib.reload(constants)
    importlib.reload(storage)
    importlib.reload(waiter)
else:
    from . import utils
    from . import email
    from . import hub
    from . import screencast
    from . import constants
    from . import storage
    from . import waiter

import bpy
import random
import os

from .screencast import FFmpegRecorder as Recorder
from .hub import Hub

from .constants import _, LocalPaths
from .storage import GoogleStorage as Storage

from bpy_extras.io_utils import ImportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty, IntProperty
from bpy.types import Operator, AddonPreferences

#   TODO
#       bug: if no new work, don't just stick with 'downloading'
#       Use md5 to avoid reup
#       Use google versions for uploads with same name
#       Add fallback to report to use online storage (instead of local cache)
#       full framing the video automatically doesn't work (bug)
#                                           (press Home as workaround)
#       audio sync/ early cutoff (bug)
#           ffmpeg flags
#           alsa ?
#           signal to stop ?
#           (current workaround is to record 5 secs after stop)
#       pip installing dependencies (or just write them down first!!!!)
#       Figure out revision logic NEXT PRIORITY
#           revision 0 initialized (move into hub)
#           expose revision parameter
#           pass into utils (done)
#           increment revision for student when reviewd or approved (done)
#       Later:
#           create summary:
#               Student, Assignment, Workshop, Milestone (maybe skip workshop)
#       Add timed review mode
# Menu:
#       Add Student -> List of students
#       Drop Student -> List of students


class AddCourse(Operator, ImportHelper):
    """ Add a course roster/ course data and build the gdrive """
    bl_idname = "wm.add_course"
    bl_label = "Add a hub course roster"

    filename_ext = ".html"

    filter_glob: StringProperty(
        default="*.html",
        options={'HIDDEN'},
        maxlen=255,
        )

    assignments: IntProperty(
        name="Assignment Count",
        description="Number of assignments in the term",
        default=4,
        )

    workshops: IntProperty(
        name="Workshop Count",
        description="Number of workshops in the term",
        default=4
        )

    milestones: IntProperty(
        name="Milestone Count",
        description="Number of milestones in the final project",
        default=4
        )

    def execute(self, context):
        Hub.add(
            self.filepath,
            self.assignments,
            self.workshops,
            self.milestones)
        return {'FINISHED'}


class BuildJSONDrives(Operator):
    """ Build the drives from the json or update them """
    bl_idname = "wm.build_drives"
    bl_label = "Build courses remote drives"

    def execute(self, context):
        """ Usually only needs to happen on test data """
        Hub.setup_test_data()
        return {'FINISHED'}


class ReportWork(Operator):
    """ Tell me who completed/which revision of a work unit """
    bl_idname = "wm.report_work"
    bl_label = "Report Assignment Stats"

    report_string: StringProperty(default="")
    report_style: EnumProperty(
        items=[
            ("current_assignment",)*3,
            ("all_assignments",)*3],
        default="current_assignment")

    @classmethod
    def poll(cls, context):
        return True

    def invoke(self, context, event):
        scene = context.scene
        course = utils.get_course(scene.course, scene.term)
        number = scene.work_number
        type = scene.work_type
        report = []
        if self.report_style == "current_assignment":
            write = False
            for student in course[_.roster]:
                reviews = student[type][number]
                if reviews == 0:
                    # find if any exist in the cache!
                    ff, fp, possibles = utils.feedback_paths(student, scene)
                    revs = [int(p.split(fp)[-1][1]) for p in possibles]
                    revs.sort()
                    revs.reverse()
                    if revs:
                        reviews = revs[0]
                        student[type][number] = reviews
                        write = True
                report.append(f"{student[_.name]}: {reviews}")
            if write:
                Hub.write()
        elif self.report_style == "all_assignments":
            for student in course[_.roster]:
                print(student[_.name])
                print(', '.join([str(count) for count in student[type]]))
                report.append(
                    f"{student[_.name]}: "
                    f"{', '.join([str(count) for count in student[type]])}")
        self.report_string = "\n".join(report)
        context.window_manager.invoke_props_dialog(self, width=400)
        return {'RUNNING_MODAL'}

    def draw(self, context):
        layout = self.layout
        for line in self.report_string.split('\n'):
            layout.label(text=line)


    def execute(self, context):
        return {'FINISHED'}




class AddWorkUnits(Operator):
    """ Add assignments to an existing course """
    bl_idname = "wm.add_work"
    bl_label = "Add work units to a course"

    assignments: IntProperty(default=4)
    workshops: IntProperty(default=4)
    milestones: IntProperty(default=4)

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, 'assignments')
        layout.prop(self.properties, 'workshops')
        layout.prop(self.properties, 'milestones')

    def invoke(self, context, event):
        scene = context.scene
        course = utils.get_course(scene.course, scene.term)
        self.assignments = course[_.assignments]
        self.workshops = course[_.workshops]
        self.milestones = course[_.milestones]
        context.window_manager.invoke_props_dialog(self, width=250)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        scene = context.scene
        Hub.update_work_counts(
            scene.term, scene.course,
            self.assignments, self.workshops, self.milestones)
        Hub.write()
        return {'FINISHED'}

class LoadFeedbackClip(Operator, ImportHelper):
    """Load a Clip for Feedback"""
    bl_idname = "wm.load_feedback_clip"
    bl_label = "Load video clip for feedback"

    # ImportHelper mixin class uses this
    filename_ext = ".mp4"

    filter_glob: StringProperty(
        default="*.mp4",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    def execute(self, context):
        utils.review_video(context, self.filepath)
        return {'FINISHED'}


class FinishFeedback(Operator):
    """Terminate and save the current screencast"""
    bl_idname = "wm.finish_feedback"
    bl_label = "Stop screencasting current feedback"
    
    def execute(self, context):
        Recorder.stop_recording()
        return {'FINISHED'}


class InitiateReview(Operator):
    """ Begin the review of a homework or assignment for a class """
    bl_idname = "wm.initate_review"
    bl_label = "Begin reviewing an assignment"

    time_limit: IntProperty(name="Time Limit", default=0)

    def execute(self, context):
        # TODO s:

        # decide where downloaded videos should be saved
        # Create download waiters in storage
        # move this whole thing into utils and just call it from here

        # Pick Course, Type (assignment/milestone/workshop), and Number
        scene = context.scene
        utils.initiate_download(
            context,
            scene.course, scene.term, scene.work_number,
            scene.work_type, scene.skip_download, scene.review)
        print("initiated")
        # change header
        bpy.types.SEQUENCER_HT_header.draw = draw_reviewing
        course = utils.get_course(scene.course, scene.term)
        student = course[_.roster][
            scene.student_downloads[scene.active_student].roster_index]
        utils.show_image(context, student)
        print("FINISHED")
        return {'FINISHED'}


class ReviewNextStudent(Operator):
    """ Review next student's work """
    bl_idname = "wm.review_student"
    bl_label = "Go to Next Student"

    use_fail_previous: BoolProperty(default=False)

    @classmethod
    def poll(cls, context):
        scene = context.scene
        active = scene.active_student
        # Needs to return True if they are all done or missing
        available = any(
            s.status == "AVAILABLE" and i != active
            for i, s in enumerate(scene.student_downloads))
        complete = all(
            s.status in ["DONE", "MISSING", "SKIP"] or i == active
            for i, s in enumerate(scene.student_downloads))
        return available or complete

    def execute(self, context):
        scene = context.scene
        course = utils.get_course(scene.course, scene.term)
        # finish up old student
        previous_student_index = scene.active_student
        previous_student_store = scene.student_downloads[previous_student_index]
        previous_student = course[_.roster][
            previous_student_store.roster_index]
        work_type = scene.work_type
        number = scene.work_number
        if Recorder.is_recording(): # if I'm reviewing the previous one, end it
            Recorder.stop_recording()
        if self.use_fail_previous:
            previous_student_store.status = "MISSING"
        if previous_student_store.status in ["REVIEWING"]:
            if os.path.isfile(previous_student_store.feedback_path):
                previous_student_store.status = "DONE"
                previous_student[work_type][number] += 1
                # TODO purge downloaded files for this student
            else:
                previous_student_store.status = "AVAILABLE" # didn't record, back to pool


        for new_idx, student_store in enumerate(scene.student_downloads):
            if (
                    student_store.status == "AVAILABLE"
                    and new_idx != scene.active_student):
                break
        else:
            # we need to set the scene state, etc.
            bpy.types.SEQUENCER_HT_header.draw = draw_initial
            scene.fb_state = "IDLE"
            storage = Storage()
            # wait for all queued uploads (block for now)
            for student_store in scene.student_downloads:
                if student_store.status == "MISSING":

                    missing_student = course[_.roster][student_store.roster_index]
                    if scene.use_email:
                        email.not_found(context, missing_student)
                elif student_store.status in ["DONE", "SKIP"]:
                    ffolder, fpath = os.path.split(student_store.feedback_path)
                    fprefix = fpath[:-4]
                    print(ffolder, fprefix, fpath)
                    if ffolder:
                        feedback_paths = [
                            f for f in os.listdir(ffolder) if fprefix in f]
                        if feedback_paths:
                            actual_student = course[_.roster][
                                student_store.roster_index]
                            if student_store.status == "DONE":
                                actual_student[work_type][number] += 1
                            print("uploading", actual_student[_.name])
                            for f in feedback_paths:
                                full_path = os.path.join(ffolder, f)
                                #TODO wrap this function
                                remote = utils.student_work_path(
                                    storage, actual_student, work_type, number)
                                remote_id = storage.add_file(
                                    full_path,
                                    remote,
                                    )
                            email.uploaded(context, actual_student)
                    else:
                        print("skipped", student_store.name)

            # final save and clear data
            Hub.write()
            scene.student_downloads.clear()
            return {'FINISHED'}
            # We are fully done


        # set the new active student
        scene.active_student = new_idx
        student = course[_.roster][
            scene.student_downloads[new_idx].roster_index]
        utils.show_image(context, student)
        scene.student_downloads[new_idx].status = "REVIEWING"
        return {'FINISHED'}


class MarkAccepted(Operator):
    """ Mark Current student as passed  even without review """
    bl_idname = "wm.accept_work"
    bl_label = "Accept Work"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        scene = context.scene
        active = scene.active_student
        course = utils.get_course(scene.course, scene.term)
        student_store = scene.student_downloads[active]
        student = course[_.roster][student_store.roster_index]
        student_store.status = 'SKIP'
        review_number = student[scene.work_type][scene.work_number] + 1
        print("review_number", review_number)
        # Set up or check the review folder
        feedback_folder, feedback_prefix, possibles = utils.feedback_paths(
            student, scene, review_number)
        if possibles:
            print('found', possibles)
            student_store.feedback_path = os.path.join(
                feedback_folder, f"{feedback_prefix}.mp4"
                )
        return {'FINISHED'}

class MarkRejected(Operator):
    """ Mark Current student as rejected even if we found work """
    bl_idname = "wm.reject_work"
    bl_label = "Reject Work"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        scene = context.scene
        active = scene.active_student
        course = utils.get_course(scene.course, scene.term)
        student_store = scene.student_downloads[active]
        student = course[_.roster][student_store.roster_index]
        student_store.status = 'MISSING'
        return {'FINISHED'}


class ReviewNextFile(Operator):
    """ Review next video or blend file """
    bl_idname = "wm.review_next"
    bl_label = "Review Next"

    @classmethod
    def poll(cls, context):
        scene = context.scene
        return (
            scene.student_downloads[scene.active_student].status in [
                "AVAILABLE", "REVIEWING"])

    def execute(self, context):

        # Get current student and course data from scene/json store
        scene = context.scene
        course = utils.get_course(scene.course, scene.term)
        student_store = scene.student_downloads[scene.active_student]
        student = course[_.roster][student_store.roster_index]
        student_store.status = "REVIEWING"
        review_number = student[scene.work_type][scene.work_number] + 1
        # Set up or check the review folder
        feedback_folder = LocalPaths.check(
            os.path.join(
                LocalPaths.reviews,
                course[_.term],
                course[_.number],
                scene.work_type,
                )
            )
        student_store.feedback_path = os.path.join(
            feedback_folder,
            f"feedback_{student[_.name]}_{scene.work_type[0].upper()}"
            f"{scene.work_number}_{review_number}.mp4"
            )

        # Get the path and email or review
        review_path = scene.reviewables
        feedback_path = student_store.feedback_path
        if os.path.isfile(feedback_path): # already recorded!
            feedback_path = feedback_path.replace(".mp4", "_p01.mp4")
            for part in range(20):
                feedback_path = f"{feedback_path[:-8]}_p{part:02}.mp4"
                print(feedback_path)
                if not os.path.isfile(feedback_path):
                    break
        if not os.path.isfile(review_path): # if no video: email
            if scene.use_email:
                email.not_found(context, student)
        elif review_path.endswith('.blend') or review_path[:-1].endswith('.blend'):
            utils.review_blend(
                context, review_path,
                feedback_path,
                use_cast= not Recorder.is_recording())
        else:
            utils.review_video(
                context, review_path,
                feedback_path,
                use_cast=not Recorder.is_recording())
        return {'FINISHED'}


class FeedbackAddonPreferences(AddonPreferences):
    bl_idname = __name__

    user: StringProperty(
        name="User",
        default="",)

    password: StringProperty(
        name="Password",
        default="",
        get=email.get_password,
        set=email.store_password
        )

    url: StringProperty(
        name="SMTP Server",
        default='smtp.hampshire.edu',
        )

    port: IntProperty(
        name="SMTP Port",
        default=587,
        )

    address: StringProperty(
        name="Address",
        default=""
    )

    def draw(self, context):
        layout = self.layout
        layout.label(text="Mail Settings")
        layout.prop(self, "address")
        layout.prop(self, "user")
        layout.prop(self, "password")
        layout.prop(self, "url")
        layout.prop(self, "port")


class StudentDownload(bpy.types.PropertyGroup):
    status: EnumProperty(
        items=[(i,) * 3 for i in (
            "IDLE","WAITING","AVAILABLE","MISSING","REVIEWING","DONE", "SKIP"
            )],
        default="IDLE"
        )
    local_paths: bpy.props.StringProperty()
    feedback_path: bpy.props.StringProperty()
    remote_id: StringProperty()
    roster_index: IntProperty()
    name: StringProperty()
    pronoun: StringProperty()


class SEQUENCER_MT_feedback(bpy.types.Menu):
    bl_label = "Extra Course Operations"

    def draw(self, context):
        layout = self.layout
        layout.operator(
            AddCourse.bl_idname, text="Add Course", icon="FILE_FOLDER")
        layout.operator(
            BuildJSONDrives.bl_idname, text="Drive Build", icon="EXPORT")
        layout.operator(
            AddWorkUnits.bl_idname,)
        layout.operator(
            ReportWork.bl_idname,
            text="Report Current").report_style = "current_assignment"
        layout.operator(
            ReportWork.bl_idname,
            text="Report All").report_style = "all_assignments"


# ############################### Registration ###############################


def __set_term__(self, value):
    self[_.term] = value
    Hub.set_current_term(value)

def __get_term__(self):
    if [_.term] in self.keys():
        return self[_.term]
    else:
        return Hub.terms.index((Hub.term,)*3)

scene_properties = {
    "skip_download": BoolProperty(default=False),
    "active_student": IntProperty(default=0),
    "student_downloads": bpy.props.CollectionProperty(
        type=StudentDownload,
        ),
    "reviewables": EnumProperty(items=waiter.get_reviewables),
    "fb_state": EnumProperty(
        items=[(state,)*3 for state in (
            "IDLE",
            "WAITING",
            "AVAILABLE",
            )],
        default="IDLE"),
    "term": EnumProperty(
        items=hub.all_terms,
        set=__set_term__,
        get=__get_term__,
        name="Current Term",
        description="Only current term courses are accessible",
        options={'SKIP_SAVE'},
        ),
    "course": EnumProperty(
        items=hub.current_term_courses,
        name="Course",
        description="Course to review",
        options={'SKIP_SAVE'},
        ),
    "work_type": EnumProperty(
        items=[(_.assignments,) * 3, (_.milestones,) * 3, (_.workshops,) * 3],
        default=_.assignments,
        name="Type",
        description="Type of Work",
        ),
    "use_email": BoolProperty(default=True),
    "work_number": IntProperty(default=1, min=1, name="Number"),
    "review": IntProperty(default=1, min=1),
    "student_store": StringProperty(default="[]"),
    "student_status": StringProperty(default="[]"),
    }

def register_properties(Type, properties):
    for key, value in properties.items():
        setattr(Type, key, value)


def unregister_properties(Type, properties):
    for prop in properties:
        delattr(Type, prop)

FULL_DRAW = bpy.types.SEQUENCER_HT_header.draw


def draw_initial(self, context):
    layout = self.layout
    scene = context.scene
    layout.template_header()

    layout.menu("SEQUENCER_MT_feedback", text='', icon='COLLAPSEMENU')
    layout.separator_spacer()
    row = layout.row(align=True)
    row.prop(scene, "term")
    row.prop(scene, "course")
    row.prop(scene, "work_type", text="")
    row.prop(scene, "work_number", text="")
    row.prop(scene, "review", text="n'th review")
    layout.separator_spacer()
    layout.prop(scene, "skip_download")
    layout.operator(
        InitiateReview.bl_idname, text="Start Review", icon="PLAY",
        )


    layout.operator(
        FinishFeedback.bl_idname, text="", emboss=False,
        icon='RADIOBUT_ON' if Recorder.is_recording()
        else 'RADIOBUT_OFF')
    layout.operator(
        LoadFeedbackClip.bl_idname, text="Load", icon="FILE_FOLDER")
    # TODO the following should be deleted from this header
    try:
        gp_layers = context.area.spaces[0].grease_pencil.layers
    except AttributeError:
        layout.label(text="No GP Layers found")
    else:
        layout.prop(gp_layers, 'active_index', text=gp_layers.active_note)
        layout.prop(gp_layers.active,'color', text="")


def draw_reviewing(self, context):
    layout = self.layout
    scene = context.scene
    active = scene.active_student
    layout.template_header()
    layout.label(text=scene.course)
    layout.label(text=scene.work_type)
    layout.label(text=str(scene.work_number))
    # TODO menu?
    layout.separator_spacer()
    # layout.label(text=scene.student_downloads[active].name)
    layout.prop(
        scene,
        "reviewables",
        text=f"{scene.student_downloads[active].name},{scene.student_downloads[active].pronoun}", icon="FILE")
    layout.operator(
        ReviewNextFile.bl_idname, text="", icon="FRAME_NEXT")
    layout.operator(
        FinishFeedback.bl_idname, text="", emboss=False,
        icon='RADIOBUT_ON' if Recorder.is_recording()
        else 'RADIOBUT_OFF')
    layout.operator(MarkAccepted.bl_idname, text="", icon="CHECKBOX_HLT")
    layout.operator(MarkRejected.bl_idname, text="", icon="CANCEL")
    layout.operator(ReviewNextStudent.bl_idname)
    layout.separator_spacer()
    layout.prop(scene, "use_email", text="Email Missing")
    try:
        gp_layers = context.area.spaces[0].grease_pencil.layers
    except AttributeError:
        layout.label(text="No GP Layers found")
    else:
        layout.prop(gp_layers, 'active_index', text=gp_layers.active_note)
        layout.prop(gp_layers.active,'color', text="")


def register():
    waiter.register()
    bpy.utils.register_class(StudentDownload)
    register_properties(bpy.types.Scene, scene_properties)
    bpy.utils.register_class(LoadFeedbackClip)

    bpy.utils.register_class(FeedbackAddonPreferences)
    bpy.utils.register_class(AddCourse)
    bpy.utils.register_class(AddWorkUnits)

    bpy.utils.register_class(MarkAccepted)
    bpy.utils.register_class(MarkRejected)

    bpy.utils.register_class(InitiateReview) # Start a Download of all assignments
    bpy.utils.register_class(ReviewNextFile) # Review next File
    bpy.utils.register_class(ReviewNextStudent)

    bpy.utils.register_class(FinishFeedback) # do we need this or just use the next

    bpy.utils.register_class(ReportWork)
    bpy.utils.register_class(BuildJSONDrives)
    bpy.utils.register_class(SEQUENCER_MT_feedback)
    bpy.types.SEQUENCER_HT_header.draw = draw_initial


def unregister():
    global FULL_DRAW
    bpy.types.SEQUENCER_HT_header.draw = FULL_DRAW
    bpy.utils.unregister_class(LoadFeedbackClip)
    bpy.utils.unregister_class(FinishFeedback)
    bpy.utils.unregister_class(AddCourse)
    bpy.utils.unregister_class(AddWorkUnits)
    bpy.utils.unregister_class(FeedbackAddonPreferences)

    bpy.utils.unregister_class(MarkAccepted)
    bpy.utils.unregister_class(MarkRejected)

    bpy.utils.unregister_class(InitiateReview)
    bpy.utils.unregister_class(ReviewNextFile)
    bpy.utils.unregister_class(ReviewNextStudent)

    bpy.utils.unregister_class(BuildJSONDrives)
    unregister_properties(bpy.types.Scene, scene_properties)
    bpy.utils.unregister_class(StudentDownload)
    bpy.utils.unregister_class(SEQUENCER_MT_feedback)
    bpy.utils.unregister_class(ReportWork)
    waiter.unregister()

if __name__ == "__main__":
    register()
