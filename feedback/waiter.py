# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali

# (a?)buse update handlers to setup timers/waiters. treat this like utils,
# can include lower level specifics (constants, storage, screencast, hub) if
# needed. can be included in utils or __init__


if "bpy" in locals():
    import importlib
    importlib.reload(storage)
    importlib.reload(constants)
    importlib.reload(screencast)

else:
    from . import storage
    from . import constants
    from . import screencast

import bpy
import multiprocessing
from multiprocessing import queues
import mimetypes
import time
import os
from .constants import _, LocalPaths
from .storage import GoogleStorage as Storage
from .screencast import FFmpegRecorder as Recorder
from bpy.props import IntProperty, BoolProperty


class Waiter():

    def __init__(self, drive, downloads):
        self.drive = drive
        self.downloads = downloads
        self.dones = [
            [False for dl in dls]
            for dls in downloads
            ] # False instead of drive.check_download(dl)
        self.done_folders = [all(d for d in dones) for dones in self.dones]
        self.errors = [ [[] for dl in dls] for dls in downloads]
        print("INITIALIZED WAITER")

    def update_done_folders(self):
        for i, done_files in enumerate(self.dones):
            self.done_folders[i] = all(d for d in done_files)

    def wait_on_files(self):
        """ Needs to check on the downloads in another thread """
        for flags, errors, folders in zip(
                self.dones, self.errors, self.downloads):
            for i, dl in enumerate(folders):
                if not flags[i]:
                    flags[i] = self.drive.check_download(dl, errors[i])
                    if len(errors[i]) > 4:
                        print('give up')
                        flags[i] = True # XXX eventually we should know...
        self.update_done_folders()

    def sequential_wait_on_files(self):
        for flags, errors, folders in zip(
                self.dones, self.errors, self.downloads):
            for i, dl in enumerate(folders):
                if not flags[i]: # reached an undownloaded file
                    flags[i] = self.drive.check_download(dl, errors[i])
                    if len(errors[i]) > 4:
                        print('GAVE UPPPP!!!!!!!!!')
                        flags[i] = True # XXX eventually we should know...
                    break # we won't continue until fully downloaded
            else:
                continue # go to the next folder
            break # wasn't done, wait till first fully downloaded
        self.update_done_folders()

    def block_on_files(self, queue):
        while not all(done for done in self.done_folders):
            self.sequential_wait_on_files()
            # print(self.done_folders)
            queue.put([d for d in self.done_folders])
            # time.sleep(5) # we don't need to be so slow

        queue.put(self.done_folders) # gotta inform that we're done...


def set_timer(context, minutes):
    """ Create an update handler to stop recording after time has passed """
    start_time = time.time()
    seconds = minutes * 60
    if context.scene.timer > 0:
        return False # can't set a timer while current one is still running
    context.scene.time_reached = False
    def handler(scene):
        if time.time() - start_time >= seconds:
            bpy.app.handlers.depsgraph_update_pre.remove(
                bpy.app.handlers.depsgraph_update_pre[-1]
                )
            Recorder.stop_recording()

    bpy.app.handlers.depsgraph_update_pre.append(handler)
    context.scene.timer = len(bpy.app.handlers.depsgraph_update_pre)
    return True


def remove_expired_timer(context):
    scene = context.scene
    if scene.timer > 0:
        Recorder.stop_recording() # just in case
        bpy.app.handlers.depsgraph_update_pre.remove(
            bpy.app.handlers.depsgraph_update_pre[context.scene.timer - 1])
        scene.timer = 0


def find_video(folder):
    """ figure out a video filepath """
    return [
        os.path.join(folder, f) for f in os.listdir(folder)
        if mimetypes.guess_type(f)[0]
        and mimetypes.guess_type(f)[0].startswith('video')
        ]


def find_blend(folder):
    return [
        os.path.join(folder, f) for f in os.listdir(folder)
        if f.endswith('.blend')]


def find_any(folder):
    return find_video(folder) or find_blend(folder)


def get_reviewables(self, context):
    scene = self
    folder = scene.student_downloads[scene.active_student].local_paths
    if scene.fb_state not in ("IDLE", "WAITING"):
        return [
            (f,) + (os.path.split(f)[-1],) * 2
            for f in find_video(folder) + find_blend(folder)]
    else:
        return[("Downloading...",) * 3]


def place_order(context, drive, downloads):
    """ Create a waiter """
    scene = context.scene
    # We're already donwnloading when this is called; prop should be set.
    waiter = Waiter(drive, [d["handles"] for d in downloads])
    qu = multiprocessing.Queue()
    def f(q, waiter):
        """ put this into other process, with some kind of passing in block """
        # waiter = Waiter(drive, [d["handles"] for d in downloads])
        waiter.block_on_files(q)
    p = multiprocessing.Process(target=f, args=(qu, waiter))
    p.start()
    scene.fb_state = 'WAITING'

    def got_it(done, idx, student_store, scene):
        print(student_store.name, done)
        if done:
            if find_any(student_store.local_paths):
                print(student_store.name, " found ", scene.fb_state)
                if scene.fb_state == 'WAITING':
                    scene.fb_state = 'AVAILABLE'
                    scene.active_student = idx # only do this the first time
                student_store.status = "AVAILABLE"
                print(scene.fb_state)
            else:
                student_store.status = "MISSING"
        return done

    def handler():
        scene = bpy.context.scene
        # waiter.wait_on_files()
        print("handling")
        try:
            done_folders = qu.get(False)
        except queues.Empty:
            print("notyet")
            return 5.0
        for idx, (student_store, done) in enumerate(zip(
                scene.student_downloads, done_folders)):
            status = student_store.status
            if status == "IDLE":
                if not got_it(done, idx, student_store, scene):
                    student_store.status = "WAITING"
            elif status == "WAITING":
                got_it(done, idx, student_store, scene)
        if all(done_folders):
            print("finished")

            return None
        return 5.0
    print("REGISTERING HANDLER")
    bpy.app.timers.register(handler)
    print("REGISTERED HANDLER")
    # bpy.app.handlers.depsgraph_update_post.append(handler)
    # scene.handler = len(bpy.app.handlers.depsgraph_update_post)
    return


def available(context):
    """ return the current student store if it is available """
    scene = context.scene
    student_store = scene.student_downloads[scene.active_student]
    if student_store.status == "AVAILABLE":
        return student_store
    else:
        return None


def next_available(context):
    """ tell if the next file is available to review """
    scene = context.scene
    for idx, student_store in enumerate(scene.student_downloads):
        if student_store.status == "AVAILABLE":
            scene.active_student = idx
            return student_store
    else:
        return None


def next_review(context):
    """  """
    return context.scene.done_next


def finished_review(context):
    """  """
    context.scene.block = True
    context.scene.done_next, context.scene.done_index = (
        False, context.scene.done_index + 1)
    context.scene.block = False


def finished_reviews(context):
    """  """
    if bpy.app.handlers.depsgraph_update_post and context.scene.handler > 0:
        index = context.scene.handler - 1
        bpy.app.handlers.depsgraph_update_post.remove(
            bpy.app.handlers.depsgraph_update_post[index])
        context.scene.handler = 0 # we don't care about other people's


def register_properties():
    bpy.types.Scene.next_student_to_review = IntProperty(default=0)
    bpy.types.Scene.reviewed_next_student = BoolProperty(default=False)
    bpy.types.Scene.done_next = BoolProperty(default=False)
    bpy.types.Scene.done_index = BoolProperty(default=False)
    bpy.types.Scene.downloading = BoolProperty(default=False)
    bpy.types.Scene.block = BoolProperty(default=False)
    # update handlers
    bpy.types.Scene.handler = IntProperty(default=0)
    bpy.types.Scene.timer = IntProperty(default=0)
    bpy.types.Scene.time_reached = BoolProperty(default=False)


def unregister_properties():
    del(bpy.types.Scene.next_student_to_review)
    del(bpy.types.Scene.reviewed_next_student)
    del(bpy.types.Scene.done_index)
    del(bpy.types.Scene.done_next)
    del(bpy.types.Scene.downloading)
    del(bpy.types.Scene.block)
    del(bpy.types.Scene.handler)
    del(bpy.types.Scene.timer)
    del(bpy.types.Scene.time_reached)


def register():
    register_properties()


def unregister():
    unregister_properties()    
