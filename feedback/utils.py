# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali

if "bpy" in locals():
    import importlib
    importlib.reload(screencast)
    importlib.reload(email)
    importlib.reload(hub)
    importlib.reload(constants)
    importlib.reload(storage)
    importlib.reload(waiter)
else:
    from . import email
    from . import screencast
    from . import hub
    from . import constants
    from . import storage
    from . import waiter

import bpy
import datetime
import json
import os
import shlex
import subprocess
import random

from .screencast import FFmpegRecorder as Recorder
from .hub import Hub
from .constants import _, LocalPaths
from .storage import GoogleStorage as Storage


def show_image(context, student):
    """ Show an image preview before casting """
    image_path = student.get(_.image_path, "")
    if not os.path.isfile(image_path):
        print(student.get(_.name, "noname?"))
        return
    scene = context.scene
    scene.sequence_editor_clear()
    scene.sequence_editor_create()
    try:
        clip = scene.sequence_editor.sequences.new_image(
            "Portrait", image_path, 1, scene.frame_current)
        clip.use_translation = True
    except:
        return


def get_dimensions(context, width, height):
    """ figure out casting dimensions based on sequencer preview """
    window = context.window
    region = context.area.regions[2]
    # Figure out letterbox
    side_box = (region.width / region.height) > (width / height)
    if side_box:
        real_height = region.height
        real_width = region.height * width // height
        offset = (region.width - real_width) // 2
        real_x = window.x + region.x + offset
        real_y = window.y + region.y
    else:
        real_width = region.width
        real_height = region.width * height // width
        offset = (region.height - real_height) // 2
        real_x = window.x + region.x
        real_y = window.y + region.y + offset
    return real_x, real_y, real_width, real_height


def screencast_cropped(context,video_path, output_path, width, height):
    """ split screencasting as an optional step """
    # Attempt to crop to student video (user should frame all!!!)
    real_x, real_y, real_width, real_height = get_dimensions(
        context, width, height)
    if output_path is None:
        review_path = f"{video_path.split('.')[0]}_{datetime.date.today()}_FB.mp4"
    else:
        review_path = output_path
    Recorder.start_recording(
        12, real_x, real_y, real_width, real_height,
        1920, 1080,
        review_path,
        )


def review_video(context, video_path, output_path=None, use_cast=True):
    screen = context.screen
    scene = context.scene
    render = scene.render

    start = 1
    channel = 1
    # Clear Annotions
    for layer in context.area.spaces[0].grease_pencil.layers:
        layer.clear()
    # Clear Timeline
    scene.sequence_editor_clear()
    # Load New Clip and Set Scene for Playback
    render.fps, render.fps_base, width, height = screencast.get_vid_props(
        video_path)
    scene.sequence_editor_create()
    clip = scene.sequence_editor.sequences.new_movie(
        "Feedback", video_path, channel, start)
    scene.frame_start, scene.frame_end = (start, clip.frame_final_duration)
    scene.frame_set(start)
    scene.render.resolution_x = width
    scene.render.resolution_y = height
    # Maximize Timeline, VSE Preview
    time_line = [
        a for a in context.screen.areas
        if a.type == 'DOPESHEET_EDITOR' and a.ui_type == 'TIMELINE']
    if time_line != []:
        time_line = time_line[0]
        bpy.ops.action.view_all(
            {'area':time_line, 'region':time_line.regions[3]})

    bpy.ops.sequencer.view_all_preview() # XXX doesn't work!
    if use_cast:
        screencast_cropped(context,video_path, output_path, width, height)


def review_blend(context, blend_path, output_path, use_cast):
    # start a second blender at a specific window size
    title = 78
    blender = "blender2.81"
    scene = context.scene
    width = scene.render.resolution_x
    height = scene.render.resolution_y
    real_x, real_y, real_width, real_height = get_dimensions(
        context, width, height)
    args = shlex.split(
        f'{blender} '
        f'-p {real_x} {real_y + title} {real_width} {real_height} '
        f'"{blend_path}"'
        ) # last added to avoid clipping audio f' -preset ultrafast -threads 0'
    print(" ".join(args))
    process = subprocess.Popen(args)
    # start ffmpeg recording that video (this is all)
    if use_cast:
        screencast_cropped(context, blend_path, output_path, width, height)
    pass


def get_course(course, term):
    """ get the right course from the hub, match term and course number """
    print(course, term)
    return [
            c for c in Hub.hub[_.courses]
            if c[_.number] == course and c[_.term] == term
            ][0]


def student_work_path(storage, student, work_type, work_number):
    return storage.id_from_path(
        student[_.id][0],
        f"{work_type.title()}/{work_type.title()[0]}{work_number}")


def initiate_download(
        context, course_str, term, number, type, skip_download, review=1):
    """ Initiate a download of folders to review an entire assignment """
    scene = context.scene
    # Step 0: Make sure previous download data is cleared XXX is this best place?
    # TODO implement this
    # Step 1: Build list of students who need review
    course = get_course(course_str, term)
    roster = course[_.roster]
    for student in roster: # XXX initialize with no reviews
        if not type in student.keys():
            student[type] = [0,] * course[type]
        while len(student[type]) < number + 1:
            student[type].append(0)
    Hub.write()
    students_idxs = [i for i, s in enumerate(roster) if s[type][number] < review]
    random.shuffle(students_idxs) # TODO make optional?
    print("students", len(students_idxs))
    # Step 2: Setup download of work to review in an update handler
    local_folder = LocalPaths.check(os.path.join(
        LocalPaths.cache,
        f"{term}/{course[_.number]}/{type}/{number}/{review}"))
    downloads = []
    if skip_download:
        for index in students_idxs:
            student = roster[index]
            local = LocalPaths.check(
                    os.path.join(local_folder, student[_.name])
                    )
            student_store = scene.student_downloads.add()
            student_store.roster_index = index
            student_store.name = student[_.name]
            student_store.pronoun = student.get("pronouns","not set")
            student_store.local_paths = local
            student_store.status = "AVAILABLE" if waiter.find_any(local) else "MISSING"
        scene.fb_state = "AVAILABLE"
    else:
        storage = Storage() # drive object for all our downloads

        for index in students_idxs:
            student = roster[index]
            local = LocalPaths.check(
                    os.path.join(local_folder, student[_.name])
                    )
            remote = student_work_path(storage, student, type, number)
            downloads.append({
                "roster_index": index,
                "local_folder": local,
                "remote_folder": remote,
                "handles": storage.download_folder(
                    remote, local, downloaders=[],
                    filetypes=[], drive_id=course[_.id][0], use_chunks=False
                    ) # we download sequentially now
                }) # XXX maybe we only need handles
            # Step 3: Initialize our state

            student_store = scene.student_downloads.add()
            student_store.roster_index = index
            student_store.name = student[_.name]
            student_store.pronoun = student.get("pronouns","not set")
            student_store.local_paths = local # TODO check later
            student_store.remote_id = remote
            student_store.status = "WAITING"
        # Step 4: Order in, waiter should wait for it to be cooked!
        print("PLACING ORDER")
        print(downloads)
        waiter.place_order(context, storage, downloads)
    return


def feedback_paths(student, scene, review_number=None):

    course = get_course(scene.course, scene.term)
    feedback_folder = LocalPaths.check(
        os.path.join(
            LocalPaths.reviews,
            course[_.term],
            course[_.number],
            scene.work_type,
            )
        )
    feedback_prefix = (
            f"feedback_{student[_.name]}"
            f"_{scene.work_type[0].upper()}{scene.work_number}"
            )
    if review_number is not None:
        feedback_prefix = (
            f"{feedback_prefix}_{review_number}"
            )
    possibles = [
        f for f in os.listdir(feedback_folder)
        if f.startswith(feedback_prefix)
        ]
    return feedback_folder, feedback_prefix, possibles
