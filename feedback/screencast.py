# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali

import signal
import shlex
import subprocess
import time


class FFmpegRecorder:
    process = None # Maybe this is dumb we need to know if we are recording
    title = 78 # Gnome titlebar height

    @classmethod
    def start_recording(
            cls, fps, lowest_x, lowest_y, width, height,
            out_width, out_height, out_file):
        """ start recording using ffmpeg assumes out_width > out_height """
        cls.stop_recording()
        # height = height - cls.title
        out_ratio = out_width / out_height
        vid_ratio = width / height
        if vid_ratio > out_ratio:
            scale_width = out_width
            scale_height = int(out_width / vid_ratio)
            pad_side = 0
            pad_top = (out_height - scale_height) // 2
        else:
            scale_height = out_height
            scale_width = int(out_height * vid_ratio)
            pad_top = 0
            pad_side = (out_width - scale_width) // 2
        # get screen height/width
        xdpyinfo = subprocess.check_output(["xdpyinfo"]).decode('utf-8')
        dims = xdpyinfo.split('dimensions:')[-1].split('pixels')[0].strip()
        screen = [int(dim) for dim in dims.strip().split('x')]
        highest_y = screen[-1] - lowest_y - height
        args = shlex.split(
            f'ffmpeg -thread_queue_size 512'
            f' -video_size {width}x{height} -framerate {fps}'
            f' -f x11grab -probesize 168M -i :1+{lowest_x},{highest_y}'
            f' -f pulse -thread_queue_size 512 -ac 1 -i default'
            f' -vf "scale={scale_width}:{scale_height}'
            f',pad={out_width}:{out_height}:{pad_side}:{pad_top}"'
            f' -s {out_width}x{out_height} "{out_file}" -y'
            ) # last added to avoid clipping audio f' -preset ultrafast -threads 0'
        print(" ".join(args))
        cls.process = subprocess.Popen(args)
        return cls.process


    @classmethod
    def stop_recording(cls):
        """ send a ctrl-C to stop the ffmpeg recording """
        if cls.is_recording():
            time.sleep(5) # XXX wait 5 seconds because audio cuts out early
            cls.process.send_signal(signal.SIGINT)
            result = cls.process.poll()
            while result is None:
                result = cls.process.poll()
            cls.process = None
            print("PROCESS EXITED WITH ",result)
            return result

    @classmethod
    def is_recording(cls):
        """ verify if we are still recording """
        return cls.process is not None


def ffprobe(video_path, cmd):
    """ Wrap ffprobe shell call to pretend I'm not being so yucky """
    args = shlex.split(cmd)
    args.append(video_path)
    return subprocess.check_output(args).decode('utf-8')


def get_vid_props(video_path):
    """
    hack with ffprobe to find video frame rate
    returns fps, fps_base, width, height
    """
    ffprobe_output = ffprobe(
        video_path,
        "ffprobe -loglevel error -select_streams v:0 -show_entries"
        " stream=width,height,r_frame_rate -of default=nw=1:nk=1").strip()
    fps, fps_base = [
        float(n) for n in ffprobe_output.split('\n')[-1].split('/')]
    width, height = [int(n.strip()) for n in ffprobe_output.split('\n')[:-1]]
    while any(f > 120 for f in (fps, fps_base)):
        fps = fps / 10
        fps_base = fps_base / 10
    return fps, fps_base, width, height
