# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
# Copyright (C) 2019 Bassam Kurdali

import bpy
import sys
import os

class _:
    """ Avoid typing strings lots of times, leads to errors """
    pass
for term in [
        "courses", "name", "ref", "type", "prefix", "subfolders", "count",
        "term", "id", "number", "roster", "pronouns", "email", "division",
        "image_path", "image_url", "advisor", "advisor_email", "assignments",
        "workshops", "milestones", "TA"]:
    setattr(_, term, term)

def __check__(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)
    return directory

class LocalPaths():
    """ helper to get relevant paths """

    @classmethod
    def check(cls, directory):
        return __check__(directory)

    __p_paths__ = (p for p in sys.path if 'python/lib/python' in p)

    scripts =  bpy.utils.script_path_user()
    lib = f"{[p for p in __p_paths__][0].split('lib/python')[0]}"
    home = os.path.expanduser('~')
    addons = __check__(os.path.join(scripts,"addons"))
    feedback = __check__(os.path.join(addons,"feedback"))
    config = __check__(os.path.join(feedback, "config"))
    cache = __check__(os.path.join(feedback, "cache"))
    images = __check__(os.path.join(config, "images"))
    reviews = __check__(os.path.join(cache, "reviews"))
    
